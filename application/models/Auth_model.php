<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {


	function auth($username, $password)
    {
        $result = 0;
        $this->db->where('username', $username);
        if ($this->db->where('password', $password))
        {
            $query = $this->db->get('user');		
            if ($query->num_rows()>0)
            {
                foreach ($query->result() as $row) {
                    $sess = array (	'username'  => $row->username,
                                    'password'  => $row->password,
                                    'user_id'   => $row->user_id,
                                    'role'      => $row->role);
                }			
                $this->session->get_userdata($sess);
                $this->session->set_userdata('user_id', $sess['user_id']);
                $this->session->set_userdata('username', $sess['username']);			
                $this->session->set_userdata('role', $sess['role']);            
                return true;                       
                        
            }else{
                return false;
                
            }
        }else
        {
            return false;
        }
		return false;
    } 
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fetchdata_model extends CI_Model {

    public function fetchdata_recti($valuesearch, $db_name){
        $id = '';
        $this->db->select("*");
        $this->db->from($db_name);           
        if($valuesearch != '')
        {
            $this->db->like('date_created', $valuesearch);
            $this->db->or_like('lp_number', $valuesearch);
            $this->db->or_like('prod_number', $valuesearch);
            $this->db->or_like('model', $valuesearch);
            $this->db->or_like('station', $valuesearch);
            $this->db->or_like('line', $valuesearch);
            $this->db->or_like('username', $valuesearch);
        }
        $this->db->order_by('date_created');
        return $this->db->get();
    }

}
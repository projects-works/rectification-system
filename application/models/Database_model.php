<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database_model extends CI_Model {

    public function save_data($table_name, $data){
        // $this->db->insert($table_name, $data);
        // return 1;
        if($this->db->insert($table_name, $data))
        {
            return true;
        }
        return false;
    }  

    public function cekuserexist($email, $username)
	{
		$this->db->where('username', $username);
		$this->db->where('email', $email);
		$query = $this->db->get('user');
		if ($query->num_rows()>0)		
		{
			return TRUE;
		}else
		{
			return FALSE;
		}
    }

    public function cekDataExist($table_name,$column_name, $model)
	{
		$this->db->where($column_name, $model);
		$query = $this->db->get($table_name);
		if ($query->num_rows()>0)		
		{
			return true;
		}else
		{
			return false;
		}
    }
    
    public function update_data($table_name, $data, $id ){
        $this->db->from($table_name);
        $this->db->where('id', $id);
        if($this->db->update($data))
        {
            return true;
        }
        return false;
    }  

    public function delete_data($table_name, $id)
    {
        $this->db->where('id', $id);
        $this->db->delete($table_name);
        return true;
    }

    public function save_year($year, $data, $table_name){
        $this->db->where('year', $year);
        $query = $this->db->get('year');
        if ($query->num_rows()<1)
		{
            $this->db->insert($table_name, $data);
        }
    }

    public function getData($table_name)
    {
        $this->db->select('*');
		$this->db->from($table_name);
		$query =$this->db->order_by('date_created')->get();
		return $query;
    }

    public function getDataAll($table_name)
    {
        $this->db->select('*');
		$this->db->from($table_name);
        $query = $this->db->get();
		return $query;
    }

    public function getDataById($table_name, $id)
    {
        $this->db->select('*');
		$this->db->from($table_name);
        $this->db->where('id', $id);
		$query =$this->db->order_by('date_created')->get();
		return $query;
    }


}
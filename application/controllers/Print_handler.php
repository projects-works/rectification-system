<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// require_once APPPATH . "third_party/dompdf/autoload.inc.php";
require_once APPPATH . "third_party/dompdfx/autoload.php";

// use Dompdf\Dompdf;
use Mpdf\Mpdf;


class Print_handler extends CI_Controller {


    public function printPdfTest1($id)
    {
        // $this->load->view('web-component/print-example-1');
        $this->load->model('database_model');
		$data = $this->database_model->getDataById('spl', $id);
        if($data->num_rows()>0)
        {
            foreach($data->result_array() as $dt)
            {
        $text = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        $html =
        '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <style>
            .table-print{
                // width: 100%;
                height: 100%;
                border-collapse:collapse;
                font-size: 1rem;
            }
            td,th{
                border: 1px solid black;
                padding: 8px;
                text-align: center;
            }
            img{
                width: 600px;
            }
            .img-info{
                max-height:400px;
            }
            .merging{
                border: 0.2px solid white;
                font-size: 0.2px;
                padding: 8;
                background-color: white;
            }
        </style>
        </head>
        <body>
        <div style=" height: fit-content; color:green; font-size:60px; position: absolute; 
        z-index: 10;left: 580px; top: 240px;"> <span style="background-color: rgba(255, 255, 255, 0.199); margin: none;"> &#9745;  </span> </div>
        <div style="color:red; font-size:60px; position: absolute; 
        z-index: 10;left: 76px; top: 240px; "> <span style="background-color: rgba(255, 255, 255, 0.199); margin: none;"> &#9746;  </span></div>
            
            <table class="table-print">
                <tr>
                    <th colspan="3" class="wid-1" style="text-align:center;">
                        <img src="'.base_url().'/assets/image/icon/mercedes-benz/mercy_logo.jpg" 
                        alt="" class="" tag="img" style="width:60px" /><br><br>
                        '.wordwrap("Mercedes-Benz", 40, '<br />', true).'
                    </th>
                    <th colspan="5" class="wid-1" style="font-size:1.7rem"> Single Point Lesson</th>
                    <th colspan="4" style="text-align:left;" class="wid-1">
                        <div>
                            <p> <span>'.wordwrap("Spl No", 20, '<br />', true).' </span> : <i>SPL_'. $id .'</i> </p>
                        </div><br>
                        <div>
                            <span>'.wordwrap("Date", 20, '<br />', true).'</span> : <i>'. $dt['date_created'].'</i>
                        </div><br>
                        <div>
                            <span>'.wordwrap("Station", 20, '<br />', true).'</span> : <i>'. $dt['station'].'</i>
                        </div><br>
                        <div>
                            <span>'.wordwrap("Reported By", 20, '<br />', true).'</span> : <i>'. $dt['username'].'</i>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th colspan="3" class="wid-1"> 
                        '.wordwrap("Category", 30, '<br />', true).'<br>
                        <i>'. $dt['category'] .'</i>
                    </th>
                    <th colspan="3" class="wid-2"> 
                        '.wordwrap("Repetative Case", 30, '<br />', true).'<br>
                        <i>'. $dt['repetative'] .'</i>
                    </th>
                    <th colspan="2" class="wid-2"> 
                        '.wordwrap("Model", 20, '<br />', true).' <br>
                        <i>'. $dt['model'] .'</i>
                    </th>
                    <th colspan="4" class="wid-1"> 
                        '.wordwrap("LP no", 20, '<br />', true).' <br>
                        <i>'. $dt['lp_number'] .'</i>
                    </th>
                </tr>   
                <tr style="border: 1px solid black;">
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                    <td colspan="1" class="merging"></td>
                </tr>
                <tr>
                    <td colspan="6" class=""> 
                        <img src="'.base_url().$dt['upload_path'].$dt['image_before'].'" 
                        alt="" class="img-info" tag="img" style="border: 2px solid red"> <br>
                        <strong><i> Wrong </i> </strong>
                    </td>
                    <td colspan="6" class=""> 
                            <img src="'.base_url().$dt['upload_path'].$dt['image_after'].'" 
                            alt="" class="img-info" tag="img" style="border: 2px solid rgb(0, 255, 255)">
                        <br>
                        <strong><i> Right </i> </strong>
                    </td>
                </tr>
                <tr>
                    <th colspan="12" class="wid-1"></th>
                </tr>
                <tr>
                    <td colspan="4" class="wid-1"> 
                    '.wordwrap("Description", 20, '<br />', true).' <br>
                    <i>'.wordwrap($text, 60, '<br />', true).' </i>
                    </td>
                    <td colspan="4" class="wid-2"> 
                        '.wordwrap("Action.", 20, '<br />', true).' <br>
                        <i>-</i>
                    </td>
                    <td colspan="1" class="wid-3">
                        <span>CS2 SPV</span><br>
                        <i>-</i>
                    </td>
                    <td colspan="1" class="wid-3">
                        CS1 SPV <br>
                        <i>-</i>
                    </td>
                    <td colspan="1" class="wid-3">
                        Operator <br>
                        <i>-</i>
                    </td>
                    <td colspan="1" class="wid-3">
                        Inspector <br>
                        <i>-</i>
                    </td>
                </tr>
            </table>

        </body>
        </html>
        ';

        }}

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
        // $mpdf->showImageErrors = true;
        $mpdf->SetDefaultFont('Arial');
        $mpdf->WriteHTML($html);
        $mpdf->Output('report.pdf', 'I');
        // $mpdf = new Mpdf();
        $mpdf->showImageErrors = true;
        $mpdf->debug = true;
        exit();
    }
	
}

<script src="<?php echo base_url(); ?>assets/general-style.js"></script>
<script src="<?php echo base_url(); ?>assets/table-style.js"></script>
<table id="table-list">
    <tr>
        <!-- <th class="table-no" onclick="sortTable(0)">No</th> -->
        <th class="table-date" onclick="sortTable(1)" title="sort by Date">Date</th>
        <th class="table-lp" onclick="sortTable(2)" title="sort by Lp Number">Lp Number</th>
        <th class="table-prod" onclick="sortTable(3)" title="sort by Prod number">Prod Number</th>
        <th class="table-model" onclick="sortTable(4)" title="sort by Model">Model</th>
        <th class="table-station" onclick="sortTable(5)" title="sort by Station">Station</th>
        <th class="table-line" onclick="sortTable(6)" title="sort by Line">Line</th>
        <th class="table-reported" onclick="sortTable(7)" title="sort by Reporter">Reported By</th>
        <th class="table-action">Action</th>
    </tr>
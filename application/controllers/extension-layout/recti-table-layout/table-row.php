<tr class="list">
    <td class="table-lp">xxxxxxxx</td>
    <td class="table-prod">xxxxxxxx</td>
    <td class="table-model">GLA 200</td>
    <td class="table-station">St. 11</td>
    <td class="table-line">Line 2</td>
    <td class="table-date">11 April 2021</td>
    <td class="table-reported">Reported By</td>
    <td class="table-action">
        <div style="width:100%">
        <div class="action-icon-box">
                <div class="flex">
                        <div class="action-icon-layout-box">
                            <a href="<?php echo site_url('Welcome/edit_spl'); ?>">
                                <img src="<?php echo base_url(); ?>assets/image/icon/edit.png" alt="" class="action-icon">
                            </a>
                        </div>
                        <div class="action-icon-layout-box">
                            <a href="<?php echo site_url('Welcome/spl_detail'); ?>">
                                <img src="<?php echo base_url(); ?>assets/image/icon/search.png" alt="" class="action-icon">
                            </a>
                        </div>
                        <div class="action-icon-layout-box">
                            <img src="<?php echo base_url(); ?>assets/image/icon/printing.png"
                            alt="" class="action-icon" 
                            onclick="window.open('<?php echo site_url('Welcome/print_spl'); ?>')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </td>
</tr>
<tr class="list">
    <td class="table-lp"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-prod"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-model"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-station"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-line"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-date"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-reported"><?php echo $dt['subject_eas'] ?></td>
    <td class="table-action">
        <div style="width:100%">
        <div class="action-icon-box">
                <div class="flex">
                        <div class="action-icon-layout-box">
                            <a href="<?php echo site_url('Welcome/edit_spl'); ?>">
                                <img src="<?php echo base_url(); ?>assets/image/icon/edit.png" alt="" class="action-icon"
                                title="edit data">
                            </a>
                        </div>
                        <div class="action-icon-layout-box">
                            <a href="<?php echo site_url('Welcome/spl_detail'); ?>">
                                <img src="<?php echo base_url(); ?>assets/image/icon/search.png" alt="" class="action-icon"
                                title="view detail data">
                            </a>
                        </div>
                        <div class="action-icon-layout-box">
                            <img src="<?php echo base_url(); ?>assets/image/icon/printing.png"
                            alt="" class="action-icon"  title="delete data"
                            onclick="window.open('<?php echo site_url('Welcome/print_spl'); ?>')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </td>
</tr>
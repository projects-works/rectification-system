<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function index()
	{
		$this->load->view('login');
	}

	public function dashboard()
	{
		$this->load->view('dashboard/dashboard');
	}
	public function submitted_issue_list()
	{
		$this->load->view('submitted_issue/submited-issue-list');
	}
	public function open_issue()
	{
		$this->load->view('submitted_issue/open-issue');
	}
	public function edit_issue()
	{
		$this->load->view('submitted_issue/edit-issue');
	}
	public function issue_detail()
	{
		$this->load->view('submitted_issue/issue-detail');
	}
	public function print_issue()
	{
		$this->load->view('submitted_issue/print-issue');
	}
	public function spl_list()
	{
		$this->load->view('spl/spl-list');
	}
	public function create_spl()
	{
		$this->load->view('spl/create-spl');
	}
	public function edit_spl()
	{
		$this->load->view('spl/edit-spl');
	}
	public function spl_detail()
	{
		$this->load->view('spl/spl-detail');
	}
	public function print_spl()
	{
		$this->load->view('spl/print-spl');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {
	
	public function index()
	{
		$this->load->view('login');
	}
	public function create_user()
	{
		$this->load->view('create-user');
	}
	public function dashboard()
	{
		$this->load->view('dashboard/dashboard');
	}
	public function submitted_issue_list()
	{
		$this->load->view('Rectification_system/submitted_issue/submited-issue-list');
	}
	public function open_issue()
	{
		$this->load->model('database_model');
		$data['model'] = $this->database_model->getDataAll('models');
		$this->load->view('Rectification_system/submitted_issue/open-issue', $data);
	}
	public function edit_issue()
	{
		$this->load->model('database_model');
		$data['info'] = $this->database_model->getData('issue');
		$this->load->view('Rectification_system/submitted_issue/edit-issue', $data);
	}
	public function issue_detail()
	{
		$this->load->model('database_model');
		$data['info'] = $this->database_model->getData('issue');
		$this->load->view('Rectification_system/submitted_issue/issue-detail', $data);
	}
	public function print_issue()
	{
		$this->load->view('Rectification_system/submitted_issue/print-issue');
	}
	public function spl_list()
	{
		$this->load->view('Rectification_system/spl/spl-list');
	}
	public function create_spl()
	{
		$this->load->model('database_model');
		$data['info'] = $this->database_model->getData('issue');
		$this->load->view('Rectification_system/spl/create-spl', $data);
	}
	public function edit_spl()
	{
		$this->load->model('database_model');
		$data['info'] = $this->database_model->getData('spl');
		$this->load->view('Rectification_system/spl/edit-spl', $data);
	}
	public function spl_detail()
	{
		$this->load->model('database_model');
		$data['info'] = $this->database_model->getData('spl');
		$this->load->view('Rectification_system/spl/spl-detail', $data);
	}
	public function print_spl()
	{
		$this->load->view('Rectification_system/spl/print-spl');
	}
	public function print_examp()
	{
		$this->load->view('web-component/print-example');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends CI_Controller {


	public function index()
	{
		$this->load->view('login');
	}

	public function auth(){
		$username 	= $this->input->post("uname");
		$password 	= $this->input->post("upass");
		$this->load->model("auth_model");
		$auth 	= $this->auth_model->auth($username, $password);
		if ($auth)
		{
			$result = 1;
		}else
		{
			$result = 0;
		}
		// $result = "authenticated";
		echo $result;
	}

	public function loged()
	{
		// if ($this->session->userdata('username') != "")
		// {                                
			redirect("activity/dashboard");
			parent.window.location.reload();     
		// }else
		// {
		// 	redirect("auth_controller");
		// }
	}

	public function logout()
	{
		$this->session->set_userdata('username', FALSE);
		$this->session->sess_destroy;
		redirect("auth_controller");
	}
}

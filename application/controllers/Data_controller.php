<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_controller extends CI_Controller {

	public function getValueSPL()
    {
        $result     = '';
        // $i          = 0;
        $valuesearch= $this->input->post("search");
        $this->load->model('fetchdata_model');
        // $result     .=  include('extension-layout/recti-table-layout/table-header.php'); 
        $result .=  '
                    <script src="'.base_url().'assets/general-style.js"></script>
                    <script src="'.base_url().'assets/table-style.js"></script>
                    <table id="table-list">
                        <tr>
                            <th class="table-date" onclick="sortTable(1)" title="sort by Date">Date</th>
                            <th class="table-lp" onclick="sortTable(2)" title="sort by Lp Number">Lp Number</th>
                            <th class="table-prod" onclick="sortTable(3)" title="sort by Prod number">Prod Number</th>
                            <th class="table-model" onclick="sortTable(4)" title="sort by Model">Model</th>
                            <th class="table-station" onclick="sortTable(5)" title="sort by Station">Station</th>
                            <th class="table-line" onclick="sortTable(6)" title="sort by Line">Line</th>
                            <th class="table-reported" onclick="sortTable(7)" title="sort by Reporter">Created By</th>
                            <th class="table-action">Action</th>
                        </tr>    
                    ';

        $data       = $this->fetchdata_model->fetchdata_recti($valuesearch, 'spl');
        if($data->num_rows()>0)
        {
            foreach($data->result_array() as $dt)
            {
                // $result .=  include("extension-layout/recti-table-layout/table-row-spl.php");
                $result .=  '
                            <tr class="list">
                                <td class="table-date">'.$dt['date_created'] .'</td>
                                <td class="table-lp">'.$dt['lp_number'] .'</td>
                                <td class="table-prod">'.$dt['prod_number'] .'</td>
                                <td class="table-model">'.$dt['model'] .'</td>
                                <td class="table-station">'.$dt['station'] .'</td>
                                <td class="table-line">'.$dt['line'] .'</td>
                                <td class="table-reported">'.$dt['username'] .'</td>
                                <td class="table-action">
                                    <div style="width:100%">
                                    <div class="action-icon-box">
                                            <div class="flex">
                                                    <div class="action-icon-layout-box">
                                                        <a href="'.base_url().'index.php/activity/edit_spl?id='.$dt['id'] .'">
                                                            <img src="'.base_url().'assets/image/icon/edit.png" alt="" class="action-icon"
                                                            title="edit data">
                                                        </a>
                                                    </div>
                                                    <div class="action-icon-layout-box">
                                                    <a href="'.base_url().'index.php/activity/spl_detail?id='.$dt['id'] .'">
                                                            <img src="'.base_url() .'assets/image/icon/search.png" alt="" class="action-icon"
                                                            title="view detail data">
                                                        </a>
                                                    </div>
                                                    <div class="action-icon-layout-box">
                                                    <a href="'.site_url('print_handler/printPdfTest1/'.$dt['id']) .'">
                                                        <img src="'.base_url().'assets/image/icon/printing.png"
                                                        alt="" class="action-icon"  title="delete data">
                                                    </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            ';
            }
        }else
        {
            // $result .=  include("extension-layout/recti-table-layout/empety-table-row.php");
            $result .= '
                        <tr class="list">
                            <td class="table-date">- - -</td>
                            <td class="table-lp"> - </td>
                            <td class="table-prod">- </td>
                            <td class="table-model"> - </td>
                            <td class="table-station"> - </td>
                            <td class="table-line"> - </td>
                            <td class="table-reported">-</td>
                            <td class="table-action">
                                <div style="width:100%">
                                <div class="action-icon-box">
                                        <div class="flex">
                                                <div class="action-icon-layout-box">
                                                    <img src="'.base_url().'assets/image/icon/edit.png" alt="" class="action-icon">
                                                </div>
                                                <div class="action-icon-layout-box">
                                                    <img src="'.base_url().'assets/image/icon/search.png" alt="" class="action-icon">                            
                                                </div>
                                                <div class="action-icon-layout-box">
                                                    <a href="'.site_url('reportpdf').'">
                                                        <img src="'.base_url().'assets/image/icon/printing.png"
                                                        alt="" class="action-icon"
                                                        >
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        ';
        }                        
        // $result     .=  include('extension-layout/recti-table-layout/table-footer.php'); 
            $result .= ' </table>';

        echo $result;
    }

    public function getValueIssue()
    {
        $result     = '';
        // $i          = 0;
        $valuesearch= $this->input->post("search");
        $this->load->model('fetchdata_model');
        // $result     .=  include('extension-layout/recti-table-layout/table-header.php'); 
        $result .=  '
                    <script src="'.base_url().'assets/general-style.js"></script>
                    <script src="'.base_url().'assets/table-style.js"></script>
                    <table id="table-list">
                        <tr>
                            <th class="table-date" onclick="sortTable(1)" title="sort by Date">Date</th>
                            <th class="table-lp" onclick="sortTable(2)" title="sort by Lp Number">Lp Number</th>
                            <th class="table-prod" onclick="sortTable(3)" title="sort by Prod number">Prod Number</th>
                            <th class="table-model" onclick="sortTable(4)" title="sort by Model">Model</th>
                            <th class="table-station" onclick="sortTable(5)" title="sort by Station">Station</th>
                            <th class="table-line" onclick="sortTable(6)" title="sort by Line">Line</th>
                            <th class="table-reported" onclick="sortTable(7)" title="sort by Reporter">Created By</th>
                            <th class="table-action">Action</th>
                        </tr>    
                    ';

        $data       = $this->fetchdata_model->fetchdata_recti($valuesearch, 'issue');
        if($data->num_rows()>0)
        {
            foreach($data->result_array() as $dt)
            {
                // $result .=  include("extension-layout/recti-table-layout/table-row-spl.php");
                $result .=  '
                            <tr class="list">
                                <td class="table-date">'.$dt['date_created'] .'</td>
                                <td class="table-lp">'.$dt['lp_number'] .'</td>
                                <td class="table-prod">'.$dt['prod_number'] .'</td>
                                <td class="table-model">'.$dt['model'] .'</td>
                                <td class="table-station">'.$dt['station'] .'</td>
                                <td class="table-line">'.$dt['line'] .'</td>
                                <td class="table-reported">'.$dt['username'] .'</td>
                                <td class="table-action">
                                    <div style="width:100%">
                                    <div class="action-icon-box">
                                            <div class="flex">
                                                    <div class="action-icon-layout-box">
                                                        <a href="'.base_url().'index.php/activity/edit_issue?id='.$dt['id'] .'">
                                                            <img src="'.base_url().'assets/image/icon/edit.png" alt="" class="action-icon"
                                                            title="edit data">
                                                        </a>
                                                    </div>
                                                    <div class="action-icon-layout-box">
                                                    <a href="'.base_url().'index.php/activity/issue_detail?id='.$dt['id'] .'">
                                                            <img src="'.base_url() .'assets/image/icon/search.png" alt="" class="action-icon"
                                                            title="view detail data">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            ';
            }
        }else
        {
            // $result .=  include("extension-layout/recti-table-layout/empety-table-row.php");
            $result .= '
                        <tr class="list">
                            <td class="table-date">- - -</td>
                            <td class="table-lp"> - </td>
                            <td class="table-prod">- </td>
                            <td class="table-model"> - </td>
                            <td class="table-station"> - </td>
                            <td class="table-line"> - </td>
                            <td class="table-reported">-</td>
                            <td class="table-action">
                                <div style="width:100%">
                                <div class="action-icon-box">
                                        <div class="flex">
                                                <div class="action-icon-layout-box">
                                                    <img src="'.base_url().'assets/image/icon/edit.png" alt="" class="action-icon">
                                                </div>
                                                <div class="action-icon-layout-box">
                                                    <img src="'.base_url().'assets/image/icon/search.png" alt="" class="action-icon">                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </td>
                        </tr>
                        ';
        }                        
        // $result     .=  include('extension-layout/recti-table-layout/table-footer.php'); 
            $result .= ' </table>';

        echo $result;
    }

    public function getValueModel()
    {
        $result     = '';
        // $i          = 0;
        $valuesearch= $this->input->post("model");
        $this->load->model('fetchdata_model');

        $result .=  '
                        <option class="model-option" value="x" readonly> Choose Model </option>
                    ';
        $data       = $this->fetchdata_model->fetchdata_recti($valuesearch, 'models');
        if($data->num_rows()>0)
        {
            foreach($data->result_array() as $dt)
            {
                // $result .=  include("extension-layout/recti-table-layout/table-row-spl.php");
                $result .=  '
                                <option class="model-option" value="'.$dt['model'].'">'.$dt['model'].'</option>
                            ';
            }
        }else
        {
            // $result .=  include("extension-layout/recti-table-layout/empety-table-row.php");
            $result .= '
                            <option class="model-option" value="x"> please create model first </option>
                        ';
        }                        
        // $result     .=  include('extension-layout/recti-table-layout/table-footer.php'); 
            $result .= ' </table>';

        echo $result;
    }
}

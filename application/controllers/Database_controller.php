<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database_controller extends CI_Controller {

	function createUser()
    {
        $username   = $this->input->post('username');
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $role       = $this->input->post('role');
        if($role != 1)
        {
            $orgnz  = $this->input->post('organization');
        }else
        {
            $orgnz  = 'admin';
        }
        $datecreated= 	date("y-m-d")."\n";

        $this->load->model('database_model');
        
        if( $username != "" && $email != "" && $password != "")
        {
            $datauser = array('username'=>$username, 'email'=>$email, 
                        'password'=>$password, 'role'=>$role, 
                        'organization' => $orgnz, 'date_created'=>$datecreated);
            $cekdatauser = $this->database_model->cekuserexist($email, $username);
            if( !$cekdatauser)
            {
                if($this->database_model->save_data('user', $datauser))
                {
                    $this->session->set_flashdata('alert', '1');	
                    redirect('activity/dashboard');  
                }else
                {
                    $this->session->set_flashdata('alert', '2');	
                    redirect('activity/create_user');
                }
            }else
            {
                $this->session->set_flashdata('alert', '2');	
                redirect('activity/create_user');
            }
        }else
        {
            $this->session->set_flashdata('alert', '2');	
            redirect('activity/create_user');
        }
	}

    public function deleteDataSPL($id)
    {
        $this->db->select('*');
		$this->db->from('spl');
        $this->db->where('id', $id);
        $data = $this->db->get();

        if($data->num_rows()>0)
        {
            foreach($data->result_array() as $dt)
            {
                $path   = $dt['upload_path'];
                $img_bfr= $dt['image_before'];
                $img_aft= $dt['image_after'];
        
            }
        }
        // $path   = $_SERVER['DOCUMENT_ROOT'].$data['upload_path'];
        $this->load->model('database_model');
        if($this->database_model->delete_data('spl',$id))
        {
            $this->deleteImg($path);
        }

        redirect('activity/spl_list');
    }

    public function deleteDataIssue($id)
    {
        $this->db->select('*');
		$this->db->from('issue');
        $this->db->where('id', $id);
        $data = $this->db->get();

        if($data->num_rows()>0)
        {
            foreach($data->result_array() as $dt)
            {
                $path   = $dt['upload_path'];
                $img    = $dt['image_issue'];
        
            }
        }
        // $path   = $_SERVER['DOCUMENT_ROOT'].$data['upload_path'];
        $this->load->model('database_model');
        if($this->database_model->delete_data('issue',$id))
        {
            $this->deleteImg($path);
        }

        redirect('activity/submitted_issue_list');
    }

    function deleteImg($path)
    {
        $files = glob($path.'*');
        foreach($files as $file)
        {
            if(is_file($file))
            {
                unlink($file);
            }
        }
    }

    function createModel()
    {
        $result = "";
        $model  = $this->input->post('model');
        
        if($model != NULL)
        {
            $data = array('model'=>$model);

            $this->load->model('database_model');
            $cekmodel   = $this->database_model->cekDataExist('models', 'model', $model);
            if (!$cekmodel)
            {
                $result     = $this->database_model->save_data('models', $data);
                
                if ($result)
                {
                    $result = 1;
                }else
                {
                    $result = 4;
                }
            }else
            {
                $result = 3;
            }
        }else
        {
            $result = 2;
        }

        echo $result;
    }

    function createSPL()
    {
        $result = FALSE;
        $i = 0;

        $userid     = $this->session->userdata('user_id');
        $username   = $this->input->post('username');
        $lp_number  = $this->input->post('lpNum');
        $prod_number= $this->input->post('proNum');
        $model      = $this->input->post('model');
        $station    = $this->input->post('station');
        $line       = $this->input->post('line');
        $description= $this->input->post('desc');
        $file_bfr   = $this->input->post('bfr');
        // $bfr        = $_FILES($file_bfr);
        
        // $aft        = $_FILES('aft');
        // $path        = 'database/assets/spl/'.$layoutid.'/';
        // $datecreated= 	date("y-m-d")."\n";
        // $this->load->model('database_model');

        // $data       = array('user_id'=>$userid, 'username'=>$username, 
        //                 'lp_number'=>$lp_number, 'prod_number'=>$prod_number, 
        //                 'model'=>$model, 'station'=>$station, 'line' => $line,
        //                 'description' => $description, 'upload_path' => $path,
        //                 'image_before' => "", 'image_after' => "");
        // $result = $this->database_model->save_data('spl', $data);
        $result = $file_bfr['name'];


        echo $result;
    }

    function createSPL2()
    {
        $result = FALSE;
        $i = 0;

        $userid     = $this->session->userdata('user_id');
        $username   = $this->input->post('username');
        // $username   = $_FILES['file-0'];
        $lp_number  = $this->input->post('lpNum');
        $prod_number= $this->input->post('proNum');
        $model      = $this->input->post('model');
        $station    = $this->input->post('station');
        $line       = $this->input->post('line');
        $description= $this->input->post('desc');
        $datecreated= 	date("y-m-d")."\n";
        
        $bfr        = $_FILES('bfr');
        $aft        = $_FILES('aft');
        // $path        = 'database/assets/spl/';

        // $file_bfr   = $bfr['file'];
        // $file_aft   = $aft['file'];

        // $config['file_name']	= "check";
        // $config['upload_path']  = $path;
        // $config['allowed_types']= 'jpg|png|jpeg';
        // $config['ecnrypt_name'] = TRUE;

        // $this->load->model('database_model');
        // $this->load->library('upload', $config);
        
        // $data       = array('user_id'=>$userid, 'username'=>$username);
                        // 'lp_number'=>$lp_number, 'prod_number'=>$prod_number, 
                        // 'model'=>$model, 'station'=>$station, 'line' => $line,
                        // 'description' => $description, 'upload_path' => $path,
                        // 'image_before' => "", 'image_after' => "");
        // $data       = array ('username' => "ok", 'model'=> "GLA 200");
        // $result = $this->database_model->save_data('spl', $data);
        // if(!$this->upload->do_upload($file_bfr))
        // {
        //     if(!$this->upload->do_upload($file_aft))
        //     {
        //         $data = $this->upload->data();
        //         $data       = array('user_id'=>$userid, 'username'=>$username, 
        //                 'lp_number'=>$lp_number, 'prod_number'=>$prod_number, 
        //                 'model'=>$model, 'station'=>$station, 'line' => $line,
        //                 'description' => $description, 'upload_path' => $path,
        //                 'image_before' => "check", 'image_after' => "check");
        //         $result = $this->database_model->save_data('spl', $data);
        //     }   
        // } 
        $result = $username;

        echo $result;
    }

    function checkSPLExist()
    {
        $result = "";
        $issueId = $this->input->post('id');

        $this->load->model('database_model');
        $cekSPL =  $this->database_model->cekDataExist('spl', 'id_issue', $issueId);
        if( !$cekSPL)
        {
            $result = 1;
        }else
        {
            $result = 2;
        }
        // $result = "check";

        echo $result;
    }

    function createSPL3($id_issue)
    {
        $result     = "";
        $category   = array();

        $userid     = $this->session->userdata('user_id');
        $username   = $this->input->post('username');
        $lp_number  = $this->input->post('lp_number');
        $prod_number= $this->input->post('prod_number');
        $model      = $this->input->post('model');
        $station    = $this->input->post('station');
        $line       = $this->input->post('line');

        $cat1       = $this->input->post('category1');
        if ($cat1 != "")
        {array_push($category,$cat1);}
        $cat2       = $this->input->post('category2');
        if ($cat2 != "")
        {array_push($category,$cat2);}
        $cat3       = $this->input->post('category3');
        if ($cat3 != "")
        {array_push($category,$cat3);}

        
        if(empty($category)){$category = "-";}
        else{$category   = implode(", ",$category);}

        if($this->input->post('repetative') != "")
        {$repetative = $this->input->post('repetative');}
        else{$repetative = "-";}

        if($this->input->post('description') != "")
        {$description=$this->input->post('description');}
        else{$description = "-";}

        $datecreated= date("y-m-d")."\n";
        $img_bfr    = $_FILES['img_bfr'];
        $img_aft    = $_FILES['img_aft']; 

        $id = $this->dataCounter('spl');
        $path       = './database/recti/spl/'.$id.'/';

        if(!is_dir('./database/recti/spl/'.$id)){
            mkdir('./database/recti/spl/'.$id,0777,TRUE);
        }

        $bfr= 'img_bfr';
        $aft= 'img_aft';

        if($_FILES['img_bfr']['size'] != 0 && $_FILES['img_aft']['size'] != 0)
        {
            $bfr_upload_name = $this->saveImg($img_bfr, $path, $id, $bfr);
            $aft_upload_name = $this->saveImg($img_aft, $path, $id, $aft);
        }else
        {
            $bfr_upload_name = "xxxxxxxxxx";
            $aft_upload_name = "xxxxxxxx";
        }
        

        $this->load->model('database_model');
        
        if ($bfr_upload_name != "" || $aft_upload_name != "")
        {
            $data       = array('user_id'=>$userid, 'id_issue' => $id_issue, 'username'=>$username,
                        'lp_number'=>$lp_number, 'prod_number'=>$prod_number, 
                        'model'=>$model, 'station'=>$station, 'line' => $line,
                        'category' =>$category, 'repetative' => $repetative,
                        'description' => $description, 'upload_path' => $path,
                        'date_created' => $datecreated,'image_before' => $bfr_upload_name, 
                        'image_after' => $aft_upload_name);

            // $dataModel  = array('model'=>$model);
            // $this->database_model->save_data('models', $dataModel);
            $result = $this->database_model->save_data('spl', $data);
            if ($result)
            {
                $this->session->set_flashdata('alert', '1');	
                redirect('activity/spl_list');
            }else
            {
                deleteImg($path);
                $this->session->set_flashdata('alert', '2');	
                redirect('activity/create_spl');
            }
            // echo $bfr_upload_name;
        }else{
            $this->session->set_flashdata('alert', '2');	
            redirect('activity/create_spl');
        }
        // echo implode(", ",$category);
    }

    function updateSPL($id)
    {
        $result     = "";
        $category   = array();

        $userid     = $this->session->userdata('user_id');
        $username   = $this->input->post('username');
        $lp_number  = $this->input->post('lp_number');
        $prod_number= $this->input->post('prod_number');
        $model      = $this->input->post('model');
        $station    = $this->input->post('station');
        $line       = $this->input->post('line');

        $cat1       = $this->input->post('category1');
        if ($cat1 != "")
        {array_push($category,$cat1);}
        $cat2       = $this->input->post('category2');
        if ($cat2 != "")
        {array_push($category,$cat2);}
        $cat3       = $this->input->post('category3');
        if ($cat3 != "")
        {array_push($category,$cat3);}

        
        if(empty($category)){$category = $this->db->query("SELECT category FROM spl WHERE id ='.$id.'");}
        else{$category   = implode(", ",$category);}

        if($this->input->post('repetative') != "")
        {$repetative = $this->input->post('repetative');}
        else
        {$repetative = $this->db->query("SELECT repetative FROM spl WHERE id ='.$id.'");}

        if($this->input->post('description') != "")
        {$description=$this->input->post('description');}
        else{$description = $this->db->query("SELECT description FROM spl WHERE id ='.$id.'");}

        $datecreated= date("y-m-d")."\n";
        $img_bfr    = $_FILES['img_bfr'];
        $img_aft    = $_FILES['img_aft']; 

        $id = $this->dataCounter('spl');
        $path       = './database/recti/spl/'.$id.'/';

        if(!is_dir('./database/recti/spl/'.$id)){
            mkdir('./database/recti/spl/'.$id,0777,TRUE);
        }

        $bfr= 'img_bfr';
        $aft= 'img_aft';

        if($_FILES['img_bfr']['size'] != 0 && $_FILES['img_aft']['size'] != 0)
        {
            $bfr_upload_name = $this->saveImg($img_bfr, $path, $id, $bfr);
            $aft_upload_name = $this->saveImg($img_aft, $path, $id, $aft);
        }else
        {
            $bfr_upload_name = $this->db->query("SELECT image_before FROM spl WHERE id ='.$id.'");
            $aft_upload_name = $this->db->query("SELECT image_after FROM spl WHERE id ='.$id.'");
        }
        

        $this->load->model('database_model');
        
        if ($bfr_upload_name != "" || $aft_upload_name != "")
        {
            $data       = array('user_id'=>$userid, 'username'=>$username,
                        'lp_number'=>$lp_number, 'prod_number'=>$prod_number, 
                        'model'=>$model, 'station'=>$station, 'line' => $line,
                        'category' =>$category, 'repetative' => $repetative,
                        'description' => $description, 'upload_path' => $path,
                        'date_created' => $datecreated,'image_before' => $bfr_upload_name, 
                        'image_after' => $aft_upload_name);

            $dataModel  = array('model'=>$model);
            $this->database_model->update_data('models', $dataModel, $id);
            $result = $this->database_model->save_data('spl', $data);
            if ($result)
            {
                $this->session->set_flashdata('alert', '1');	
                redirect('activity/spl_list');
            }else
            {
                deleteImg($path);
                $this->session->set_flashdata('alert', '2');	
                redirect('activity/create_spl');
            }
            // echo $bfr_upload_name;
        }else{
            $this->session->set_flashdata('alert', '2');	
            redirect('activity/create_spl');
        }
        // echo implode(", ",$category);
    }

    function reportissue()
    {
        $result = "";

        $userid     = $this->session->userdata('user_id');
        $username   = $this->input->post('username');
        $lp_number  = $this->input->post('lp_number');
        $prod_number= $this->input->post('prod_number');
        $model      = $this->input->post('model');
        $station    = $this->input->post('station');
        $line       = $this->input->post('line');
        $description= $this->input->post('description');
        $datecreated= date("y-m-d")."\n";
        $img_file   = $_FILES['img'];

        $id = $this->dataCounter('issue');
        $path       = './database/recti/issue/'.$id.'/';

        if(!is_dir('./database/recti/issue/'.$id)){
            mkdir('./database/recti/issue/'.$id,0777,TRUE);
        }

        $img    = 'img';
        // $this->cropImage($img);

        if($_FILES['img']['size'] != 0)
        {
            $img_upload_name = $this->saveImg($img_file, $path, $id, $img);
        }else
        {
            $img_upload_name = "";
        }
        

        $this->load->model('database_model');
        
        if ($img_upload_name != "")
        {
            $data       = array('user_id'=>$userid, 'username'=>$username,
                        'lp_number'=>$lp_number, 'prod_number'=>$prod_number, 
                        'model'=>$model, 'station'=>$station, 'line' => $line,
                        'description' => $description, 'upload_path' => $path,
                        'date_created' => $datecreated,'image_issue' => $img_upload_name);
            $dataModel  = array('model'=>$model);
            $this->database_model->save_data('models',  $dataModel);
            $result = $this->database_model->save_data('issue', $data);
            if ($result)
            {
                $this->session->set_flashdata('alert', '1');	
                redirect('activity/submitted_issue_list');
            }else
            {
                deleteImg($path);
                $this->session->set_flashdata('alert', '2');	
                redirect('activity/open_issue');
            }
        }else
        {
            $this->session->set_flashdata('alert', '2');	
            redirect('activity/open_issue');
        }
        // echo $img_upload_name;
    }

    function saveImg($img, $path, $id, $file)
    {
        $name_file  = '';

        $config['upload_path']  = $path;
        $config['allowed_types']= 'jpg|png|jpeg';
        $config['ecnrypt_name'] = TRUE;
        $config['file_name']    = $id.'_'.$file;

        $this->load->library('upload',$config);

        if ($this->upload->do_upload($file))
        {
            $data = $this->upload->data();
            
            if($data)
            {
                $name_file = $this->upload->data('file_name');
            }
        }
        
        return $name_file;
    }

    function dataCounter($table_name)
    {
        $this->db->select('*');
		$this->db->from($table_name);
        $data = $this->db->get();
        $qty_data = $data->num_rows()+1;

        return $qty_data;
    }

    function cropImage($image_src)
    {
        $image = imagecreatefromjpeg($_GET[$image_src]);
        $filename = $image_src;

        $thumb_width = 200;
        $thumb_height= 150;
        
        $width  = imagesx($image);
        $height = imagesy($image);

        $original_aspect= $width/$height;
        $thumb_aspect   = $thumb_width/$thumb_height;

        if ($original_aspect >= $thumb_aspect)
        {
            $new_height = $thumb_height;
            $new_width  = $width / ($width / $thumb_width);
        }else
        {
            $new_width = $thumb_width;
            $new_height= $height / ($height / $thumb_height);
        }

        $thumb = imagecreatetruecolor( $thumb_width, $thumb_height);

        imagecopyresampled( $thumb,
                            $image,
                            0 - ($new_width - $thumb_width) / 2,
                            0 - ($new_height - $thumb_height) / 2,
                            0, 0,
                            $new_width, $new_height,
                            $width, $height);

        return imagejpeg($thumb, $filename, 80);
    }
}

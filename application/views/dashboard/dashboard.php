<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">

        <?php 
         define("BASE_URL", "application/views/web-component/");
         include(BASE_URL ."header-navbar.php");?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div class="flex-md" style="width:100%">

                            <a href="<?php echo site_url('activity/submitted_issue_list'); ?>">

                                <div class="dashboard-menu-layout-box">
                                    <div class="dashboard-menu-body-2 ">
                                        <div class="dashboard-menu-title-2">
                                            <img src="<?php echo base_url() ?>assets/image/icon/settings.png" alt=""
                                            class="icon-dashboard-menus">
                                        </div>
                                    </div>
                                    <div class="menu-title">
                                        <span>Rectification System</span>
                                    </div>
                                </div>
                            </a>
                            <!-- <a href="<?php echo site_url('activity/submitted_issue_list'); ?>">
                                <div class="dashboard-menu-layout-box">
                                    <div class="dashboard-menu-body">
                                        <div class="dashboard-menu-title">
                                            <span>Submitted Issue</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            
                            <a href="<?php echo site_url('activity/spl_list'); ?>">
                                <div class="dashboard-menu-layout-box">
                                    <div class="dashboard-menu-body">
                                        <div class="dashboard-menu-title">
                                            <span>SPL</span>
                                        </div>
                                    </div>
                                </div>
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include(BASE_URL ."footer.php");?>
    </div>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>    
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div>
        <div>
            <div>
                <div>
                    <div class="main-center-body">
                        <div class="main-center">
                            <div class="element-box col-xl-2 col-lg-3 col-sm-5 col-es-6">
                                <div class="form-box col-xl-8 col-lg-7 col-sm-8 col-es-8">
                                    <div>
                                        <div class="title-form-box">
                                            <span class="title-form"> Login </span>
                                            <div id="info-login" style="display:none;">
                                                <br>
                                                <span class="font-size-5" style="font-family:montserrat; color:red;"> 
                                                    please refresh the page, if the page not refreshing automaticly.
                                                </span>
                                            </div>
                                        </div>
                                        <div>
                                            <form action="">
                                                <div class="input-layout-box">
                                                    <input type="text" name="username" id="username" class="input-box-rounded"
                                                    placeholder="Username" require>
                                                </div>
                                                <div class="input-layout-box">
                                                    <input type="password" name="password" id="password" class="input-box-rounded"
                                                    placeholder="Password" require>
                                                </div>
                                                <div class="submit-layout-box">
                                                    <!-- <a href="<?php echo site_url('Welcome/dashboard'); ?>"> -->
                                                        <button type="submit" id="login-button" class="button-box-rounded">Login</button>
                                                    <!-- </a> -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<?php echo base_url();?>assets/jquery/jquery.js"></script>
<script>
    $(document).ready(function()
    {

        $("#login-button").on('click', function()
        {
            var uname = $("#username").val();
            var upass = $("#password").val();
            if (uname != "" && upass != "")
            {
                login(uname,upass);
                // alert("check");
            }else{
                alert("please fill out the form.");
            }
        });


        function login(uname,upass)
        {

            $.ajax({
                type    : "POST",
                url     : "<?php echo site_url('auth_controller/auth'); ?>",
                data    : {uname:uname, upass:upass},
                success : function(data)
                {
                    // alert(data)
                    if(data == 1)
                    {
                         <?php
                            if ($this->session->userdata('username') != "")
                            {
                                redirect("activity/dashboard");
                                parent.window.location.reload();     
                            }
                         ?>
                    }else
                    {
                        alert("please check your username or password.");
                    }
                    
                }
            });

            function infoLogin()
            {
                $('#info-login').css("display", "block");
            }
        }
    });
</script>
</html>
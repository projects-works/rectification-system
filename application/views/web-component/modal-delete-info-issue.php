<div class="modal" id="modal-delete">
    <div class="modal-bg">
        <div>
            <div>
                <div class="main-center-body">
                    <div class="main-center">
                        <div>
                            <div class="modal-body col-xl-3">
                                <div class="modal-inner-body col-xl-9">
                                    <div>
                                        <span class="font-size-5">Are you sure want to delete this information?</span>
                                    </div>
                                    <div>
                                        <div>
                                            <div class=" flex" style="margin-top: 16%;">
                                                <div class="col-xl-6">
                                                    <a href="<?php echo site_url('database_controller/deleteDataIssue/'.$id); ?>">
                                                        <button class="button-modal yes-modal">
                                                            Delete
                                                        </button>
                                                    </a>
                                                </div>
                                                <div class="col-xl-6">
                                                    <button class="button-modal cancel" id="cancel-delete-info">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
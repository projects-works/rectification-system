<div class="modal" id="modal-create-model">
    <div class="modal-bg">
        <div>
            <div>
                <div class="main-center-body">
                    <div class="main-center">
                        <div>
                            <div class="modal-body col-xl-3">
                                <div class="modal-inner-body col-xl-9">
                                    <div>
                                        <span class="font-size-5">Create Model</span>
                                    </div>
                                    <div>
                                        <div>
                                            <div>
                                                <form action="">
                                                    <div class="input-layout-box">
                                                        <div class="col-md-7 col-sm-6 col-es-12" style="margin:auto;">
                                                            <input type="text" name="model" id="model" 
                                                            class="input-box-underlined" require placeholder="Type model here..">
                                                        </div>
                                                    </div>
                                                
                                            </div>
                                            <div class=" flex" style="margin-top: 16%;">
                                                <div class="col-xl-6">
                                                    <button class="button-modal yes-modal" id="create-model-btn">
                                                        create
                                                    </button>
                                                </div>
                                                </form>
                                                <div class="col-xl-6">
                                                    <button class="button-modal cancel" id="cancel-create-model">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


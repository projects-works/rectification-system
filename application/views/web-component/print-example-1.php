<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/print.css"> -->
    <style>
        .table-box{
            width: 100%;
        }
        .table-print{
        width: 100%;
        /* background-color: black; */
        }
        th,td{
            padding: 8px;
            border: 1px solid black;
        }
        th.print{
            font-size: medium;   
        }
        td.print{
            font-size: medium;    
        }
    </style>
</head>
<body>
    <div class="table-box">
    <table class="table-print">
        <tr>
            <th colspan="4" class="wid-1">
            <img src="'.base_url().'/assets/image/icon/mercedes-benz/mercy_logo.png" 
            alt="" class="img-info" tag="img"/>
             '.wordwrap("Mercedes-Benz", 40, '<br />', true).'
            </th>
            <th colspan="4" class="wid-1"> Single Point of Lesson</th>
            <th colspan="4" style="text-align:left;" class="wid-1">
                <div>
                    <p> <span>'.wordwrap("Spl No", 20, '<br />', true).' </span> :  </p>
                </div><br>
                <div>
                '.wordwrap("Date:", 20, '<br />', true).'
                </div><br>
                <div>
                '.wordwrap("Station:", 20, '<br />', true).'
                </div><br>
                <div>
                '.wordwrap("Reported BY:", 20, '<br />', true).'
                </div>
            </th>
        </tr>
        <tr>
            <th colspan="4" class="wid-1"> '.wordwrap("Category", 20, '<br />', true).'</th>
            <th colspan="2" class="wid-2"> '.wordwrap("Repetitive Case", 20, '<br />', true).'</th>
            <th colspan="2" class="wid-2"> '.wordwrap("Model", 20, '<br />', true).'</th>
            <th colspan="4" class="wid-1"> '.wordwrap("Valid from: + LP no", 20, '<br />', true).'</th>
        </tr>
        <tr>
            <td colspan="6" class="main-center wid-4"> 
                <div >
                    <img src="'.base_url().'/assets/image/dummy/IMG_7960.JPG" 
                    alt="" class="img-info" tag="img"/>
                </div>
            </td>
            <td colspan="6"class="main-center wid-4"> 
                <div>
                    <img src="'.base_url().'/assets/image/dummy/IMG_7960.JPG" 
                    alt="" class="img-info" tag="img"/>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="wid-1"> 
            '.wordwrap("Description", 20, '<br />', true).' <br>
            '.wordwrap($text, 60, '<br />', true).' 
            </td>
            <td colspan="4" class="wid-2"> 
                '.wordwrap("Action.", 20, '<br />', true).' <br>
                adsadasd
            </td>
            <td colspan="1" class="wid-3"><span>CS2 SPV</span></td>
            <td colspan="1" class="wid-3">CS1 SPV</td>
            <td colspan="1" class="wid-3">Operator</td>
            <td colspan="1" class="wid-3">Inspector</td>
        </tr>
        <tr>
            
        </tr>
    </table>

    </div>
   
</body>
</html>
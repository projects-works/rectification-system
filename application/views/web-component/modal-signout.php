<script src="<?php echo base_url();?>assets/jquery/jquery.js"></script>
<?php 
    if ($this->session->userdata('username')== '') 
    {	
        $message = "during innactivity, you're logout";
        for ($i = 0; $i<2; $i++)
        {
            if($i == 0)
            {
                echo "<script type='text/javascript'> alert('$message') </script>";
            }else
            {
                redirect('auth_controller/logout');     
            }
        } 
    }
?> 
<div class="modal" id="modal-logout">
    <div class="modal-bg" id="modal-bg">
        <div>
            <div>
                <div class="main-center-body">
                    <div class="main-center">
                        <div>
                            <div class="modal-body col-xl-3 col-lg-3 col-sm-3 col-es-3">
                                <div class="modal-inner-body col-xl-9 col-lg-9">
                                    <div>
                                        <span class="font-size-5">Are you sure want to logout?</span>
                                    </div>
                                    <div>
                                        <div>
                                            <div class=" flex" style="margin-top: 16%;">
                                                <div class="col-xl-6">
                                                    <a href="<?php echo site_url('auth_controller/logout'); ?>">
                                                        <button class="button-modal yes-modal logout" onclick="">
                                                            Logout
                                                        </button>
                                                    </a>
                                                </div>
                                                <div class="col-xl-6">
                                                    <button class="button-modal cancel" onclick="closeModalLogout()">
                                                        cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
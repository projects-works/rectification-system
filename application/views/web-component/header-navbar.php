
<?php 
    include("modal-signout.php");
?>
<div class="header-navbar-body">
            <div class="container">
                <div>
                    <div>
                        <div class="left">
                            <div>
                                <button class="button-box-navbar" onclick="location.href = '<?php echo site_url('Welcome/dashboard'); ?>'"> Dashboard </button>
                            </div>
                        </div>
                        <div class="right">
                            <div>
                                <div class="flex">
                                    <div class="username-navbar-box">
                                        <span class="content-navbar"><?php echo $this->session->userdata('username')?></span>
                                    </div>
                                    <div class="content-navbar-box">
                                        <img src="<?php echo base_url() ?>assets/image/icon/user icon.png" alt=""
                                        class="icon-user dropdown-button" onclick="openDropdownMenu()">
                                    </div>
                                </div>
                            </div>
                            <div id="dropdown-menu" class="dropdown-content">
                                <div class="dropdown-menus">
                                    Edit account <div class="icon-dropdown"></div>
                                </div>
                                <div class="dropdown-menus" id="admin">
                                    <a href="<?php echo site_url('Activity/create_user'); ?>">
                                        Add User <div class="icon-dropdown"></div>
                                    </a>
                                </div>
                                <div class="dropdown-menus">
                                    <span onclick="openModalLogout()">Logout</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
<script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
<script>
    checkadmin(<?php echo $this->session->userdata('role')?>);
</script>
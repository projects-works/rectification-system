<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create User</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">
    <script src="<?php echo base_url(); ?>assets/js/alert.js"></script> 
    <script> alertMessage(<?php echo $_SESSION['alert'] ?>); </script>
    
        <?php 
            define("BASE_URL", "application/views/web-component/");
            include(BASE_URL ."header-navbar.php");
        ?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div>
                            <a href="<?php echo site_url('activity/dashboard'); ?>">
                                <button class="button-box-half-rounded ">
                                    <span class="arrow-back"> &#129032;  </span>
                                    Back
                                </button>
                            </a>
                        </div>
                        <div>
                            <div class="element-box  col-xl-8 col-es-10">
                                <div class="form-box col-xl-5 col-md-6 col-sm-8 col-es-8">
                                    <div class="title-form-box">
                                        <span class="title-form"> Create User </span>
                                    </div>
                                    <div class="input-gnrl" style="text-align:left">
                                        <span class="required"> <strong>*</strong></span> is require.
                                    </div> <br>
                                    <div>
                                        <?php echo form_open_multipart('database_controller/createUser'); ?>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Username <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="username" id="username" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Password<span class="required"> <strong>*</strong></span></Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="password" name="password" id="password" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Email<span class="required"> <strong>*</strong></span></Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="email" name="email" id="email" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Role<span class="required"> <strong>*</strong></span></Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="role" id="role" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/role-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Organization<span class="required"> <strong>*</strong></span></Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="organization" id="organization" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/organization-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="submit-layout-box-submit">
                                                <a href="<?php echo site_url('Welcome/dashboard'); ?>">
                                                    <button type="submit" 
                                                    class="button-box-half-rounded-sbumit" id="from-btn">
                                                        Create User
                                                    </button>
                                                </a>
                                            </div>
                                        <?php echo form_close();?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
            include(BASE_URL ."footer.php");
        ?> 
    </div>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
    
    <!-- <script src="<?php echo base_url(); ?>assets/js/spl-form.js"></script> -->
</body>
</html>
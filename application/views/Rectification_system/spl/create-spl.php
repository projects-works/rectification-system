<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create SPL</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">
    <script src="<?php echo base_url(); ?>assets/js/alert.js"></script> 
    <script> alertMessage(<?php echo $_SESSION['alert'] ?>); </script>
    
        <?php 
            define("BASE_URL", "application/views/web-component/");
            include(BASE_URL ."header-navbar.php");
            $id = $_GET['id'];
            foreach($info->result_array() as $data){ 
                $lp_num     = $data['lp_number'];
                $prod_num   = $data['prod_number'];
                $model      = $data['model'];
                $line       = $data['line'];
            }
        ?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div>
                            <a href="<?php echo site_url('activity/spl_list'); ?>">
                                <button class="button-box-half-rounded ">
                                    <span class="arrow-back"> &#129032;  </span>
                                    Back
                                </button>
                            </a>
                        </div>
                        <div>
                            <div class="element-box  col-xl-8 col-es-10">
                                <div class="form-box col-xl-5 col-md-6 col-sm-8 col-es-8">
                                    <div class="title-form-box">
                                        <span class="title-form"> Create SPL </span>
                                    </div>
                                    <div class="input-gnrl" style="text-align:left">
                                        <span class="required"> <strong>*</strong></span> is require.
                                    </div> <br>
                                    <div>
                                    <?php echo form_open_multipart('database_controller/createSPL3/'.$id); ?>
                                    <?php 
                                        foreach($info->result_array() as $data){ 
                                            if ($data['id'] == $id)
                                            {
                                        ?>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Username</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="username" id="username" 
                                                    class="input-box-underlined" readonly require value="<?php echo $data['username'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">LP Number</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="lp_number" id="lp_number" 
                                                    class="input-box-underlined" require value="<?php echo $data['lp_number'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Prod Number</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="prod_number" id="prod_number" 
                                                    class="input-box-underlined" require value="<?php echo $data['prod_number'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Model</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="model" id="model" 
                                                    class="input-box-underlined" require 
                                                    value="<?php echo $data['model'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Line</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="line" id="line" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/line-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Station</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="station" id="station" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/station-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Category</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <div>
                                                        <div class="left input-gnrl">
                                                            <input type="checkbox" name="category1" id="category1" 
                                                            class="input-gnrl" value="Basic Knowledge" > 
                                                            <label for="category1"> Basic Knowledge</label>
                                                        </div>
                                                            
                                                        <div class="left input-gnrl">
                                                            <input type="checkbox" name="category2" id="category2" 
                                                            class="input-gnrl" value="Imporvement" >
                                                            <label for="category2"> Imporvement</label> 
                                                        </div>
                                                        <div class="left input-gnrl">
                                                            <input type="checkbox" name="category3" id="category3" 
                                                            class="input-gnrl" value="Trouble Case" >
                                                            <label for="category3"> Trouble Case</label>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Repetative Case</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <div >
                                                        <div class="left input-gnrl">
                                                            <input type="radio" name="repetative" id="repetative1" 
                                                            class="input-gnrl"  value="yes">
                                                            <label for="category3"> yes </label> <br>
                                                        </div>

                                                        <div class="left input-gnrl">
                                                            <input type="radio" name="repetative" id="repetative2" 
                                                            class="input-gnrl"  value="no" checked>
                                                            <label for="category3"> no </label> <br>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <script src="<?php echo base_url(); ?>assets/js/retrieve-data.js"></script>
                                            <script>
                                            retrieveDataCreateSpl(
                                                "<?php echo $data['line'] ?>", 
                                                "<?php echo $data['station'] ?>");</script>
                                            
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Description</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">

                                                    <textarea name="description" id="description" cols="30" 
                                                    rows="6" class="input-box-underlined" 
                                                    value="<?php echo $data['description'] ?>"></textarea>
                                                    
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input"> Photo Before</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="file" name="img_bfr" id="img_bfr" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input"> Photo After</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <div>
                                                        <input type="file" name="img_aft" id="img_aft" 
                                                        class="input-box-underlined" require>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            
                                            <div class="submit-layout-box-submit">
                                                <a href="<?php echo site_url('activity/dashboard'); ?>">
                                                    <button type="submit" 
                                                    class="button-box-half-rounded-sbumit">
                                                        Save 
                                                    </button>
                                                </a>
                                            </div>
                                            <?php }} ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
            include(BASE_URL ."footer.php");
        ?> 
    </div>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/manage-form-option.js"></script>
    
    <!-- <script src="<?php echo base_url(); ?>assets/js/spl-form.js"></script> -->

    <script>
        $(document).ready(function()
        {
            var pros = {
            pro1: '#img_bfr',
            pro2: '#img_bfr'
            };

        $('#img_bfr, #img_aft').on('click', function()
            {
               alert("Recomended file to upload :\n Orientation = Landscape\n File max size = 10mb") ;
            });
        })
    </script>
</body>
</html>
<html lang="en"><head>
    <title>Print SPL</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <style Type="">
        table{
            width: 100%;
        }
        td{
            font-family: montserrat;
            word-break: break-all;
            border: 1px;
        }
        tr{
            background-color: grey;
        }
        .print-table{
            border: 2px solid rgb(0, 41, 41);
            background-color: grey;
            border-radius: 2px;
            text-align: left;
            padding: 0.4rem 0.8rem 0.4rem 0.8rem;
        }
        .table-title{
            // border: 2px solid black;
            font-family: montserratSemiBold;
            background-color: white;
            // color: white
        }
        .table-label-print{
            font-family: montserrat;
            @extend .font-size-5;
        }
        .table-field-print{
            font-family: montserratSemiBold;
            color: rgb(0, 0, 0);
            @extend .font-size-5;
            margin: 2px 0 2px 0;
        }
    </style>
</head><body>
<?php 
    foreach($info->result_array() as $data){
?>
    <table class="print" >
        <tr>
            <td colspan="8" class="table-title" >
                <span class="font-size-2"> Single Point Lesson</span>
            </td>
        </tr>
        <tr>
            <td colspan="8" class="table-title"></td>
        </tr>
        <tr>
            <td colspan="2" class="print-table">
                <div>
                    <span class="table-label-print">Model :</span>
                </div>
                <div>
                    <p class="table-field-print"><?php echo $data['model'] ?></p>
                </div>
            </td>
            <td colspan="2" class="print-table">
                <div>
                    <span class="table-label-print">PM :</span>
                </div>
                <div>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>
            </td>
            <td colspan="2" class="print-table">
                <div>
                    <span class=" table-label-print">LP Number :</span>
                </div>
                <div>
                    <p class="table-field-print"><?php echo $data['lp_number'] ?></p>
                </div>
            </td>
            <td colspan="2" class="print-table">
                <div>
                    <span class=" table-label-print">Station :</span>
                </div>
                <div>
                    <p class="table-field-print"><?php echo $data['station'] ?></p>
                </div>
            </td>
        </tr>
        <tr class="photo">
            <td colspan="1"></td>
            <td colspan="6" style="max-width: 100%;" class="">
                <div class="element-box col-xl-8 col-md-10 col-sm-10 col-es-8">
                    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/assets/image/dummy/IMG_7960.JPG'?> 
                    alt="" class="img-info" tag="img"/>
                    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'\assets/image/dummy/IMG_7960.JPG'?> 
                    alt="" class="img-info" tag="img"/>
                    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'./assets/image/dummy/IMG_7960.JPG'?> 
                    alt="" class="img-info" tag="img"/>
                    <div class="caption-image-layout">
                        <!-- <span class="caption-image font-size-5">Before</span> -->
                    </div>
                </div>
            </td>
            <td colspan="1"></td>
        </tr>
        <tr>
            <td colspan="4" class="print-table" style="word-break:break-all;">
                <div>
                    <span class=" table-label-print">Description : </span> <br>
                    <p class="table-field-print">
                        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                    </p>
                    
                </div>
            </td>
            <td colspan="2" class="print-table">               
                <div>
                    <span class=" table-label-print">Date Created :</span> <br>
                    <p class="table-field-print"><?php echo $data['date_created'] ?></p>
                </div>                
            </td>
            <td colspan="2" class="print-table">               
                <div>
                    <span class=" table-label-print">Created By:</span> <br>
                    <p class="table-field-print"><?php echo $data['username'] ?></p>
                </div>
            </td>
        </tr>
    </table>
<?php } ?>
</body></html>
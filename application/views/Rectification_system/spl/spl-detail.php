<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Issue Detail</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">

        <?php 
        $id = $_GET['id'];
         define("BASE_URL", "application/views/web-component/");
         include(BASE_URL ."header-navbar.php");
         include(BASE_URL ."modal-delete-info-spl.php");
         ?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div>
                            <div>
                                <div class="left">
                                    <a href="<?php echo site_url('Activity/spl_list'); ?>">
                                        <button class="button-box-half-rounded ">
                                            <span class="arrow-back"> &#129032;  </span>
                                            Back
                                        </button>
                                    </a>
                                </div>
                                <div class="right">
                                    <div class="flex">
                                        <div class="action-icon-layout-box">
                                            <a href="<?php echo  base_url()?>index.php/activity/edit_spl?id=<?php echo $id ?>">
                                                <img src="<?php echo base_url(); ?>assets/image/icon/edit.png" alt="" class="action-icon">
                                            </a>
                                        </div>
                                        <div class="action-icon-layout-box">
                                            <a href="<?php echo site_url('print_handler/printPdfTest1/'.$id); ?>">
                                                <img src="<?php echo base_url(); ?>assets/image/icon/printing.png"
                                                    alt="" class="action-icon">
                                            </a>
                                        </div>
                                        <div class="action-icon-layout-box">
                                                <img src="<?php echo base_url(); ?>assets/image/icon/trash.png"
                                                    alt="" class="action-icon" id="delete-info">
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="title-page-box">
                            <span>SPL Details</span>
                        </div>

                        <?php 
                            foreach($info->result_array() as $data){
                                if ($data['id'] == $id)
                                {      
                                    $url    = $data['upload_path'];
                                    $img_bfr= $data['image_before'];
                                    $img_aft= $data['image_after'];
                        ?>
                        <div>
                            <div>
                                <div>
                                    <div class="flex-md">
                                        <div class="element-box before col-xl-4 col-md-6 col-sm-8 col-es-10">
                                            <img src="<?php echo base_url($url.$img_bfr); ?>" 
                                            alt="" class="img-info">
                                            <div class="caption-image-layout" style="bottom: 0;">
                                                <span class="caption-image font-size-5">Before</span>
                                            </div>
                                        </div>

                                        <div class="element-box after col-xl-4 col-md-6 col-sm-8 col-es-10">
                                            <img src="<?php echo base_url($url.$img_aft); ?>" 
                                            alt="" class="img-info">
                                            <div class="caption-image-layout" style="bottom: 0;">
                                                <span class="caption-image font-size-5">After</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="box-information">
                                    <div class="element-box col-xl-6 col-md-8 col-sm-10 col-es-12">
                                        <div class="container">
                                            <div class="flex">
                                                <div class="col-xl-5 col-md-5 col-sm-5 col-es-5">
                                                    <div class=" col-xl-12 col-md-12">
                                                        <div class="col-xl-12 col-es-12">
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8 col-sm-7">
                                                                    <span class="label-info">Lp Number</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                    <span class="value-info font-size-4">
                                                                        <?php echo $data['lp_number'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Prod Number</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                    <span class="value-info font-size-4">
                                                                        <?php echo $data['prod_number'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Reported By</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                <span class="value-info font-size-4">
                                                                        <?php echo $data['username'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Description</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                <span class="value-info font-size-4">
                                                                        <?php echo $data['description'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-2 col-md-2 col-sm-2 col-es-1"><div style=" height: 100%; width: 1px; background-color: black; margin: auto;"></div></div>

                                                <div class="col-xl-5 col-md-5 col-sm-5 col-es-5">
                                                    <div class=" col-xl-12 col-md-12">
                                                        <div class="col-xl-12 col-es-12">
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Model</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                <span class="value-info font-size-4">
                                                                        <?php echo $data['model'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Station</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                <span class="value-info font-size-4">
                                                                        <?php echo $data['station'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Line</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                <span class="value-info font-size-4">
                                                                        <?php echo $data['line'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="box-text flex-md">
                                                                <div class="col-xl-7 col-md-8  col-sm-7 ">
                                                                    <span class="label-info font-size-4">Date</span>
                                                                </div>
                                                                <div class="col-xl-5 col-md-4 col-sm-5">
                                                                <span class="value-info font-size-4">
                                                                        <?php echo $data['date_created'];?>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <?php }} ?>
                        <div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                footer
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/general-style.js"></script>
</body>
</html>
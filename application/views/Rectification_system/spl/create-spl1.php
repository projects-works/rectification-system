<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create SPL</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">

        <?php 
         define("BASE_URL", "application/views/web-component/");
         include(BASE_URL ."header-navbar.php");?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div>
                            <a href="<?php echo site_url('Welcome/spl_list'); ?>">
                                <button class="button-box-half-rounded ">
                                    <span class="arrow-back"> &#129032;  </span>
                                    Back
                                </button>
                            </a>
                        </div>
                        <div>
                            <div class="element-box  col-xl-8 col-es-10">
                                <div class="form-box col-xl-5 col-md-6 col-sm-8 col-es-8">
                                    <div class="title-form-box">
                                        <span class="title-form"> Create SPL </span>
                                    </div>
                                    <div>
                                        <form action="">
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Username</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="username" id="username" 
                                                    class="input-box-underlined" disabled require 
                                                    value="<?php echo $this->session->userdata('username')?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">LP Number</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="lp_number" id="lp_number" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Prod Number</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="prod_number" id="prod_number" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Model</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="model" id="model" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Station</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="station" id="station" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/station-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Line</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="username" id="username" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/line-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Description</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <!-- <input type="text" name="description" id="description" 
                                                    class="input-box-underlined" require > -->
                                                    <textarea name="description" id="description" cols="30" rows="6" class="input-box-underlined"></textarea>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input"> Photo Before</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="file" name="img_bfr" id="img_bfr" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input"> Photo After</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <div>
                                                        <input type="file" name="img_aft" id="img_aft" 
                                                        class="input-box-underlined" require>
                                                    </div>
                                                    <div>
                                                        <button style="position:absolute;">upload</button>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            
                                            <div class="submit-layout-box-submit">
                                                <a href="<?php echo site_url('Welcome/dashboard'); ?>">
                                                    <button type="submit" 
                                                    class="button-box-half-rounded-sbumit" id="from-btn">
                                                        Create SPL
                                                    </button>
                                                </a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                footer
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
    <script>
        // var url =;
        // var url = "check";
        $(document).ready(function()
    {

        $("#from-btn").on('click', function()
        {
            var uname   = $("#username").val();
            var lpNum   = $("#lp_number").val();
            var prodNum = $("#prod_number").val();
            var model   = $("#model").val();
            var station = $("#station").val();
            var line    = $("#line").val();
            var desc    = $("#description").val();
            var bfr     = $("#img_bfr").val();
            var aft     = $("#img_aft").val();

            // if (uname != "" && lpNum != "" && prodNum !="" && model != "" 
            // && station != "x" && line != "x")
            // {
                createSPL(uname ,lpNum,prodNum,model,station,line,desc,bfr,aft);
                // alert("check");
            // }else{
            //     alert("please fill out the form.");
            // }
        });


        function createSPL(uname,lpNum,prodNum,model,station,line,desc,bfr,aft)
        {

            $.ajax({
                type    : "POST",
                url     : "<?php echo site_url('database_controller/createSPL'); ?>",
                data    : {uname:uname, lpNum:lpNum, prodNum:prodNum, model:model, 
                    station:station, line:line, desc:desc, bfr:bfr, aft:aft},
                success : function(data)
                {
                    alert(data);
                    // if(data == 1)
                    // {
                    //     alert("spl created."); 
                    // }else
                    // {
                    //     alert("create spl failed.");
                    // }
                    
                }
            });
        }

    });
    </script>
    <!-- <script src="<?php echo base_url(); ?>assets/js/spl-form.js"></script> -->
</body>
</html>
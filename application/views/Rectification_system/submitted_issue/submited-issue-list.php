<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Submitted Issue</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
<div class="main-body">
<script> alertMessage("<?php echo $_SESSION['alert']; ?>");</script>
    <script src="<?php echo base_url(); ?>assets/js/alert.js"></script> 
        <?php 
            define("BASE_URL", "application/views/web-component/");
            include(BASE_URL ."header-navbar.php");
            include(BASE_URL ."modal-create-model.php");
            ?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div class="flex">
                            <div class="menu-rectification-box">
                                <a href="<?php echo site_url('activity/open_issue'); ?>">
                                    <button class="button-box-half-rounded "> Report issue </button>
                                </a>
                            </div>
                            <div class="menu-rectification-box" >
                                <a href="<?php echo site_url('activity/spl_list'); ?>">
                                    <button class="button-box-half-rounded " style="background:black; color:white;"> SPL List </button>
                                </a>
                            </div>
                            <div class="menu-rectification-box">
                                <button class="button-box-half-rounded " id="create-model"> New Model </button>
                            </div>
                        </div>
                        
                        <div class="title-page-box">
                            <span>Issue List</span>
                        </div>

                        <div class="table-tools-box-layout">
                            <div class="left">
                                <div class="entries-data">
                                    <span>Show </span>
                                    <select name="" id="maxRows">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="5000">show all</option>
                                    </select>
                                    <span> entries</span>
                                </div>
                            </div>
                            <div class="right">
                                <input type="text" name="searchBox" id="searchBox" 
                                class="input-box-underlined" placeholder="Search">
                            </div>
                            <div class="clear"></div>
                        </div>

                        
                        <div class="table-box" id="resultSearch">
                            <!-- <table id="table-list">
                                
                            </table> -->
                        </div>

                        <div class="table-tools-box-layout">
                            <div class="left"></div>
                            <div class="right">
                                <div>
                                    <nav>
                                        <ul class="pagination">
                                            <li data-page="prev">
                                                <span> &#10094; </span>
                                            </li>

                                            <li data-page="next" id="prev">
                                                <span> &#10095; </span>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                footer
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/jquery/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
    <script src="<?php echo base_url(); ?>assets/table-style.js"></script>
    <script>
    $(document).ready(function()
    {
        search = "";
        getDataSPL(search);
        $("#searchBox").keyup(function(){
            search = $("#searchBox").val();    
            getDataSPL(search);
            // alert(search);
        })

        function getDataSPL(search)
        {
            // alert(search);
            $.ajax({
                type: "POST",
                url : "<?php echo site_url('data_controller/getValueIssue') ?>",  
                data: { search:search},              
                success: function(data)
                {                                         
                    $('#resultSearch').html(data);   
                    // alert(data);
                }
            })
        }       
        
        $("#create-model-btn").on('click', function()
        {
            var model   = $("#model").val();
            // alert(model);
            if (model != " ")
            {
                $.ajax({
                type    : "POST",
                url     : "<?php echo site_url('database_controller/createModel'); ?>",
                data    : {model:model},
                success : function(data)
                {
                    // alert(data);
                    if(data == 1)
                    {
                        alert("model created."); 
                        $("#modal-create-model").style.display ="none";
                    }else if(data == 2)
                    {
                        alert("fill out the form.");
                    }else if(data == 3)
                    {
                        alert("model already created");
                    }else if(data == 4)
                    {
                        alert("insert to database failed.")
                    }
                    // data = "";
                }
            });
            }else{
                alert("please fill out the form.");
            }
        }); 

    });

    
</script>
</body>
</html>
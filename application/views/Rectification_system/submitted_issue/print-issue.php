<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print Issue</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    
<table class="print">
        <tr>
            <td colspan="8" class="table-title" >
                <span class="font-size-2"> Issue</span>
            </td>
        </tr>
        <tr>
            <td colspan="8" class="table-title"></td>
        </tr>
        <tr>
            <td colspan="2" class="print-table">
                <div>
                    <span class="table-label-print">Type :</span>
                </div>
                <div>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>
            </td>
            <td colspan="2" class="print-table">
                <div>
                    <span class="table-label-print">PM :</span>
                </div>
                <div>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>
            </td>
            <td colspan="2" class="print-table">
                <div>
                    <span class=" table-label-print">LP Number :</span>
                </div>
                <div>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>
            </td>
            <td colspan="2" class="print-table">
                <div>
                    <span class=" table-label-print">Station :</span>
                </div>
                <div>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>
            </td>
        </tr>
        <tr class="photo">
            <td colspan="1"></td>
            <td colspan="6" style="max-width: 100%;" class="">
                <div class="element-box col-xl-8 col-md-10 col-sm-10 col-es-8">
                    <img src="<?php echo base_url(); ?>assets/image/dummy/IMG_7960.JPG" 
                    alt="" class="img-info">
                    <!-- <img src="<?php echo $_SERVER["DOCUMENT_ROOT"].'./assets/image/dummy/IMG_7960.JPG'?> 
                    alt="" class="img-info" tag="img"/> -->
                    <div class="caption-image-layout">
                        <!-- <span class="caption-image font-size-5">Before</span> -->
                    </div>
                </div>
            </td>
            <td colspan="1"></td>
        </tr>
        <tr>
            <td colspan="4" class="print-table">
                <div>
                    <span class=" table-label-print">Description : </span> <br>
                    <p class="table-field-print">
                        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                    </p>
                    
                </div>
            </td>
            <td colspan="2" class="print-table">               
                <div>
                    <span class=" table-label-print">Date Created :</span> <br>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>                
            </td>
            <td colspan="2" class="print-table">               
                <div>
                    <span class=" table-label-print">Created By:</span> <br>
                    <p class="table-field-print">xxxxxxxxxxxx</p>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>
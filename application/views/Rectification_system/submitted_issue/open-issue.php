<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Open Issue</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">
    <script src="<?php echo base_url(); ?>assets/js/alert.js"></script> 
    <script> alertMessage(<?php echo $_SESSION['alert'] ?>); </script>
        <?php 
         define("BASE_URL", "application/views/web-component/");
         include(BASE_URL ."header-navbar.php");?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div>
                            <a href="<?php echo site_url('activity/submitted_issue_list'); ?>">
                                <button class="button-box-half-rounded ">
                                    <span class="arrow-back"> &#129032;  </span>
                                    Back
                                </button>
                            </a>
                        </div>
                        <div>
                            <div class="element-box  col-xl-8 col-es-10">
                                <div class="form-box col-xl-5 col-md-6 col-sm-8 col-es-8">
                                    <div class="title-form-box">
                                        <span class="title-form"> Report Issue </span>
                                    </div>
                                    <div class="input-gnrl" style="text-align:left">
                                        <span class="required"> <strong>*</strong></span> is require.
                                    </div> <br>
                                    <div>
                                    <?php echo form_open_multipart('database_controller/reportIssue'); ?>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Username <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="username" id="username" 
                                                    class="input-box-underlined" readonly require 
                                                    value="<?php echo $this->session->userdata('username')?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">LP Number <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="lp_number" id="lp_number" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Prod Number <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="prod_number" id="prod_number" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Model <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <!-- <input type="text" name="model" id="model" 
                                                    class="input-box-underlined" require> -->
                                                    <select name="model" id="model" onchange="checkline()" 
                                                    class="input-box-underlined" require>
                                                    <option class="model-option" value="x" dissabled> Choose Model </option>
                                                        <?php 
                                                            foreach($model->result_array() as $data){ 
                                                        ?>
                                                            <option class="model-option" value="<?php echo $data['model'] ?>" > <?php echo $data['model'] ?> </option>
                                                        <?php 
                                                            }
                                                            // else
                                                            // {
                                                            // <option class="model-option" value="x"> Choose Model </option>
                                                            // }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Line <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="line" id="line" onchange="checkline()" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/line-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Station <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="station" id="station" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/station-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Description</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <!-- <input type="text" name="description" id="description" 
                                                    class="input-box-underlined" require > -->
                                                    <textarea name="description" id="description" cols="30" rows="6" class="input-box-underlined"></textarea>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input"> Photo <span class="required"> <strong>*</strong></span> </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="file" name="img" id="img" 
                                                    class="input-box-underlined" require>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            
                                            <div class="submit-layout-box-submit">
                                                <button type="submit" 
                                                class="button-box-half-rounded-sbumit" id="from-btn">
                                                    Create SPL
                                                </button>
                                            </div>
                                            <?php echo form_close();?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                footer
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/manage-form-option.js"></script>

    <script>
        $(document).ready(function()
        {
            getDataModels();

            $('#img').on('click', function()
            {
               alert("Recomended file to upload :\n Orientation = Landscape\n File max size = 10mb") ;
            });

            function getDataModels()
            {
                // alert(search);
                var model = "";
                $.ajax({
                    type: "POST",
                    url : "<?php echo site_url('data_controller/getValueModel') ?>",  
                    data: { model:model},              
                    success: function(data)
                    {                                         
                        $('#modelResult').html(data);   
                        // alert(data);
                    }
                })
            }     
        });
    </script>
</body>
</html>
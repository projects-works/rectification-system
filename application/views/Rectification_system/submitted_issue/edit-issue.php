<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Open Issue</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/customize-stylish.me.css">
    <link rel="icon" href="<?php echo base_url(); ?>assets/image/icon/mercedes-benz/mercy_logo.png" type="image/gif">
</head>
<body>
    <div class="main-body">

        <?php 
         define("BASE_URL", "application/views/web-component/");
         include(BASE_URL ."header-navbar.php");?>

        <div class="body-content">
            <div class="container">
                <div class="container">
                    <div>
                        <div>
                            <a href="<?php echo site_url('activity/submitted_issue_list'); ?>">
                                <button class="button-box-half-rounded ">
                                    <span class="arrow-back"> &#129032;  </span>
                                    Back
                                </button>
                            </a>
                        </div>
                        <div>
                            <div class="element-box  col-xl-8 col-es-10">
                                <div class="form-box col-xl-5 col-md-6 col-sm-8 col-es-8">
                                    <div class="title-form-box">
                                        <span class="title-form"> Edit Issue </span>
                                    </div>
                                    <div>
                                    <form action="">
                                        <?php 
                                        foreach($info->result_array() as $data){ 
                                        ?>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Username</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="username" id="username" 
                                                    class="input-box-underlined" disabled require placeholder="<?php echo $data['username'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">LP Number</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="lp_number" id="lp_number" 
                                                    class="input-box-underlined" require placeholder="<?php echo $data['lp_number'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Prod Number</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="prod_number" id="prod_number" 
                                                    class="input-box-underlined" require placeholder="<?php echo $data['prod_number'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Model</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="text" name="model" id="model" 
                                                    class="input-box-underlined" require placeholder="<?php echo $data['model'] ?>">
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Line</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="line" id="line" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/line-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Station</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <select name="station" id="station" 
                                                    class="input-box-underlined" require>
                                                        <?php include(BASE_URL ."data/station-list.php"); ?>
                                                    </select>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <script src="<?php echo base_url(); ?>assets/js/retrieve-data.js"></script>
                                            <script>
                                            retrieveDataIssue(
                                                "<?php echo $data['line'] ?>", 
                                                "<?php echo $data['station'] ?>");</script>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input">Description</Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">

                                                    <textarea name="description" id="description" cols="30" 
                                                    rows="6" class="input-box-underlined" 
                                                    placeholder="<?php echo $data['description'] ?>"></textarea>
                                                    
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="input-layout-box">
                                                <div class="left">
                                                    <Span class="label-input"> Photo </Span>
                                                </div>
                                                <div class="right col-md-7 col-sm-6 col-es-12">
                                                    <input type="file" name="evidence" id="evidence" 
                                                    class="input-box-underlined" require placeholder="check">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                          
                                            <div class="submit-layout-box-submit">
                                                <a href="<?php echo site_url('activity/dashboard'); ?>">
                                                    <button type="submit" 
                                                    class="button-box-half-rounded-sbumit">
                                                        Save 
                                                    </button>
                                                </a>
                                            </div>
                                            <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                footer
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/general-style.js"></script>
</body>
</html>
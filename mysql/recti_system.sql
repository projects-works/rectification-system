-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Okt 2021 pada 07.19
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recti_system`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `issue`
--

CREATE TABLE `issue` (
  `id` int(100) NOT NULL,
  `lp_number` int(100) NOT NULL,
  `prod_number` int(100) NOT NULL,
  `model` varchar(50) NOT NULL,
  `station` varchar(50) NOT NULL,
  `line` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `upload_path` varchar(200) NOT NULL,
  `image_issue` varchar(200) NOT NULL,
  `date_created` date NOT NULL,
  `user_id` int(200) NOT NULL,
  `username` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `issue`
--

INSERT INTO `issue` (`id`, `lp_number`, `prod_number`, `model`, `station`, `line`, `description`, `upload_path`, `image_issue`, `date_created`, `user_id`, `username`) VALUES
(7, 1234512, 123412, 'A 35', 'station 01', 'line 01', '123132123', './database/recti/issue/1/', '1_img.PNG', '2021-10-23', 1, 'admin'),
(8, 1234512, 123412, 'A 35', 'station 04', 'line 02', '1123123123', './database/recti/issue/2/', '2_img.PNG', '2021-10-24', 1, 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `models`
--

CREATE TABLE `models` (
  `id` int(200) NOT NULL,
  `model` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `models`
--

INSERT INTO `models` (`id`, `model`) VALUES
(2, 'A 35'),
(6, 'GLA 200'),
(8, 'GLC 200'),
(9, 'GLE 450'),
(10, 'GLS 450'),
(11, 'C 200'),
(12, 'C 300'),
(13, 'A 200'),
(14, 'GLA 35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `organization`
--

CREATE TABLE `organization` (
  `id` int(200) NOT NULL,
  `organization` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `spl`
--

CREATE TABLE `spl` (
  `id` int(100) NOT NULL,
  `id_issue` int(100) NOT NULL,
  `lp_number` int(100) NOT NULL,
  `prod_number` int(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  `station` varchar(100) NOT NULL,
  `line` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `repetative` varchar(10) NOT NULL,
  `date_created` date NOT NULL,
  `user_id` int(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `upload_path` varchar(200) NOT NULL,
  `image_before` varchar(200) NOT NULL,
  `image_after` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `spl`
--

INSERT INTO `spl` (`id`, `id_issue`, `lp_number`, `prod_number`, `model`, `station`, `line`, `category`, `repetative`, `date_created`, `user_id`, `username`, `description`, `upload_path`, `image_before`, `image_after`) VALUES
(6, 7, 1234512, 123412, 'A 35', 'station 00', 'line 01', 'Basic Knowledge, Trouble Case', '-', '2021-10-24', 1, 'admin', '-', './database/recti/spl/1/', '1_img_bfr.PNG', '1_img_bfr1.PNG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `station_responsibility`
--

CREATE TABLE `station_responsibility` (
  `id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `station` varchar(200) NOT NULL,
  `line` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date_created` date NOT NULL,
  `organization` varchar(50) NOT NULL,
  `role` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `date_created`, `organization`, `role`) VALUES
(1, 'admin', 'admin', '-', '0000-00-00', 'admin', 1),
(4, 'gajah', '123', 'gajahrizki@gmail.com', '2021-10-21', 'Production', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `issue`
--
ALTER TABLE `issue`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spl`
--
ALTER TABLE `spl`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `station_responsibility`
--
ALTER TABLE `station_responsibility`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `issue`
--
ALTER TABLE `issue`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `models`
--
ALTER TABLE `models`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `spl`
--
ALTER TABLE `spl`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `station_responsibility`
--
ALTER TABLE `station_responsibility`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

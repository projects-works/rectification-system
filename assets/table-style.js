coloringRows();

getPagination('#table-list');

function getPagination(table)
{
    var lastPage = 1;

    $('#maxRows')
        .on('change', function(evt)
        {
            lastPage = 1;
            $('.pagination')
                .find('li')
                .slice(1, -1)
                .remove();
            var trnum = 0;
            var maxRows = parseInt($(this).val());

            if (maxRows == 5000)
            {
                $('.pagination').hide();
            }else
            {
                $('.pagination').show();
            }

            var totalRows = $(table + ' tbody tr').length;
            $(table + ' tr:gt(0)').each(function()
            {
                trnum++;
                if (trnum > maxRows)
                {
                    $(this).hide();
                }
                if(trnum <= maxRows)
                {
                    $(this).show();
                }
            });
            if (totalRows > maxRows)
            {
                var pagenum = Math.ceil(totalRows / maxRows);

                for (var i = 1; i<=pagenum;)
                {
                    $('.pagination #prev')
                        .before(
                            '<li data-page="' + i + '"> ' + i++ + ' </li>'
                        )
                        .show();
                }
            }
            $('.pagination [data-page="1"]').addClass('active');
            $('.pagination li').on('click', function(evt)
            {
                evt.stopImmediatePropagation();
                evt.preventDefault();
                var pagenum = $(this).attr('data-page');

                var maxRows = parseInt($('#maxRows').val());

                if(pagenum == 'prev')
                {
                    if(lastPage == 1)
                    {
                        return;
                    }
                    pagenum = --lastPage;
                }
                if(pagenum == 'next')
                {
                    if(lastPage == $('.pagination li').length - 2)
                    {
                        return;
                    }
                    pagenum = ++lastPage;
                }
                lastPage = pagenum;
                var trIndex = 0;

                $('.pagination li').removeClass('active');
                $('.pagination [data-page="' + lastPage +'"]').addClass('active');

                limitPagging();
                $(table + ' tr:gt(0)').each(function()
                {
                    trIndex++;
                    if(trIndex > maxRows * pagenum || trIndex <= maxRows * pagenum - maxRows)
                    {
                        $(this).hide();
                    }else
                    {
                        $(this).show();
                    }
                });
            });
            limitPagging();
        })
        .val(5)
        .change();
}

function limitPagging()
{
    if($('.pagination li').length > 7)
    {
        if($('.pagination li.active').attr('data-page') <= 3)
        {
            $('.pagination li;gt(5)').hide();
            $('.pagination li;lt(5)').show();
            $('.pagination [data-page="next"]').show();
        }
        if($('.pagination li.active').attr('data-page') >3)
        {
            $('.pagination li;gt(0)').hide();
            $('.pagination [data-page="next"]').show();
            for (let i = (parseInt($('.pagination li.active').attr('data-page')) -2);
             i <= (parseInt($('.pagination li.active').attr('data-page')) + 2); i++)
             {
                 $('.pagination [data-page='+ i +'"]').show();
             }
        }
    }
}

$(function()
{
    $('table tr:eq(0)').prepend('<th class="table-no" onclick="sortTable(0)"> No </th>');
    var id = 0;

    $('table tr:gt(0)').each(function()
    {
        id++;
        $(this).prepend('<td class="table-no">' + id + '</td>');
    });
});





function coloringRows()
{ 
    row = document.getElementsByClassName("list");
    // row[1].style.backgroundColor = "rgb(221, 231, 231)";

    // row = document.getElementsByTagName("TR");
    // alert(document.getElementsByClassName('list').length);
    for (var i = 0; i<row.length; i++)
    {
        if(i % 2 !=0)
        {
            row[i].style.backgroundColor = "rgb(221, 231, 231)";
        }
        
    }
}

function sortTable(position)
{
    var table, rows, switcing, i, x, y, shouldSwitch;
    table = document.getElementById("table-list");
    switching = true;

    while(switching)
    {
        switching = false;
        rows = table.rows;

        for (i = 1; (i<rows.length - 1); i++)
        {
            shouldSwitch = false;

            x = rows[i].getElementsByTagName("TD")[position];
            y = rows[i + 1].getElementsByTagName("TD")[position];
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase())
            {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch)
        {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}
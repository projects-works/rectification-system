function openDropdownMenu()
{
    var dropdown = document.getElementById("dropdown-menu");
    if(!dropdown.classList.contains('show'))
    {
        document.getElementById("dropdown-menu").classList.toggle("show");
    }
}

window.onclick = function(event){
    if (!event.target.matches('.dropdown-button'))
    {
        if (!event.target.matches('.dropdown-content'))
        {
            if (!event.target.matches('.dropdown-menus'))
            {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for(i=0; i < dropdowns.length; i++)
                {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show'))
                    {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
    }
}

function openModalLogout()
{
    // document.getElementById("modal-logout").classList.toggle("sho2");
    document.getElementById("modal-logout").style.display ="block";
    // alert("check");
}
$('.logout').mouseover(function()
{
    // document.getElementById("modal-bg").classList.toggle("modal-hover");
    document.getElementById("modal-bg").style.backgroundColor = "rgba(0, 0, 0, 0.6)";
    document.getElementById("modal-bg").style.transition = "0.6s";
})
$('.logout').mouseout(function()
{
    // document.getElementById("modal-bg").classList.toggle("modal-hover");
    document.getElementById("modal-bg").style.backgroundColor = "rgba(100, 100, 100, 0.5)";
    document.getElementById("modal-bg").style.transition = "0.2s";
})
function closeModalLogout()
{
    // document.getElementById("modal").classList.remove("show2");
    document.getElementById("modal-logout").style.display ="none";
}

$('#delete-info').on('click', function()
{
    // document.getElementById("modal-delete").classList.remove("show2");
    document.getElementById("modal-delete").style.display ="block";
})

$('#cancel-delete-info').on('click', function()
{
    // document.getElementById("modal-delete").classList.remove("show2");
    document.getElementById("modal-delete").style.display ="none";
})

$('#create-model').on('click', function()
{
    // document.getElementById("modal-delete").classList.remove("show2");
    document.getElementById("modal-create-model").style.display ="block";
})

$('#cancel-create-model').on('click', function()
{
    // document.getElementById("modal-delete").classList.remove("show2");
    document.getElementById("modal-create-model").style.display ="none";
})